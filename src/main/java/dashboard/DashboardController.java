package dashboard;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import login.Login;
import signUp.SignUp;

public class DashboardController {
	@FXML
	private Button signup;
	@FXML
	private Button login;
	
	public void signup(ActionEvent event) {
		new SignUp().show();
		
	}
	public void login(ActionEvent event) {
		new  Login().show();
	}
}
