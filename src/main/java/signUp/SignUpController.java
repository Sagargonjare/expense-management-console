package signUp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ResourceBundle;

import dashboard.Dashboard;
import friends.Friends;
import group.Group;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class SignUpController /*implements Initializable*/{
	
	@FXML
	private TextField fullName;
	

	@FXML
	private TextField email;

	@FXML
	private TextField mobile;

	@FXML
	private TextField password;

	@FXML
	private ComboBox currency;

	@FXML
	private ComboBox country;

	@FXML
	private ComboBox language;

	@FXML
	private Button signUp;

	@FXML
	private Button back;
	

	public void signUp(ActionEvent event) throws IOException {
		  final String messageContent = "{\n" + 
					"\"fullName\"" + ":\"" + fullName.getText() + "\", \r\n" +
					"\"email\"" + ":\"" + email.getText() + "\", \r\n" +
					"\"mobile\"" + ":\""+ mobile.getText() + "\", \r\n" + 
					"\"password\"" + ":\"" + password.getText() + "\", \r\n" +
					"\"country\"" + ":\"" + country.getValue() + "\", \r\n" +
					"\"currency\"" + ":\""+ currency.getValue() + "\", \r\n" + 
					"\"language\"" + ":\"" + language.getValue() + "\" \r\n" +
					"\n}";

	System.out.println(messageContent);

	String link = "http://localhost:8080/directory/api/v1/signup";
	URL urlObj = new URL(link);
	HttpURLConnection postCon = (HttpURLConnection) urlObj.openConnection();
	postCon.setRequestMethod("POST");
	postCon.setRequestProperty("userId", "abcdef");
	postCon.setRequestProperty("Content-Type", "application/json");
	postCon.setDoOutput(true);

	OutputStream osObj = postCon.getOutputStream();
	osObj.write(messageContent.getBytes());

	osObj.flush();
	osObj.close();
	int respCode = postCon.getResponseCode();
	System.out.println("Response from the server is: \n");
	System.out.println("The POST Request Response Code :  " + respCode);
	System.out.println("The POST Request Response Message : " + postCon.getResponseMessage());
	if (respCode == HttpURLConnection.HTTP_CREATED) {
	
	InputStreamReader irObj = new InputStreamReader(postCon.getInputStream());
	BufferedReader br = new BufferedReader(irObj);
	String input = null;
	StringBuffer sb = new StringBuffer();
	while ((input = br.readLine()) != null) {
	sb.append(input);
	}
	br.close();
	postCon.disconnect();
	System.out.println(sb.toString());
	new  Group().show();
	} else {
	System.out.println("POST Request did not work.");
	}
	}
	public void back(ActionEvent event) {
		new  Dashboard().show();
	}
//	public void initialize(URL location, ResourceBundle resources) {
//		ObservableList<String> list = FXCollections.observableArrayList("Dollar", "Rupee");
//		currency.setItems(list);
//		ObservableList<String> list1 = FXCollections.observableArrayList("India", "Rasia", "America", "Shrilanka",
//				"Aushtrelia", "Japan", "Europ", "NewZeland", "England");
//		country.setItems(list1);
//		ObservableList<String> list2 = FXCollections.observableArrayList("Marathi", "English", "Hindi");
//		currency.setItems(list2);
//		
//		
//	}
}
	
