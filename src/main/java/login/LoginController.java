package login;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import dashboard.Dashboard;
import friends.Friends;
import group.Group;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController {
	@FXML
	private TextField email;
	@FXML
	private TextField mobile;
	@FXML
	private PasswordField password;
	@FXML
	private Button back;
	@FXML
	private Button login;
	
	
		public void login(ActionEvent event) throws IOException {
			final String messageContent = "{\n" + "\"email\"" + ":\"" + email.getText() + "\", \r\n" + "\"mobile\"" + ":\""
					+ mobile.getText() + "\", \r\n" + "\"password\"" + ":\"" + password.getText() + "\" \r\n" + "\n}";
			
			System.out.println(messageContent);

			String link = "http://localhost:8080/directory/api/v1/validate";
			URL url = new URL(link);
			HttpURLConnection postCon = (HttpURLConnection) url.openConnection();
			postCon.setRequestMethod("POST");
			postCon.setRequestProperty("userId", "abcdef");
			
			postCon.setRequestProperty("Content-Type", "application/json");
			postCon.setDoOutput(true);
			
			OutputStream os = postCon.getOutputStream();
			os.write(messageContent.getBytes());
			
			os.flush();
			os.close();
			int respCode = postCon.getResponseCode();
			System.out.println("Response from the server is: \n");
			System.out.println("The POST Request Response Code :  " + respCode);
			System.out.println("The POST Request Response Message : " + postCon.getResponseMessage());
			if (respCode == HttpURLConnection.HTTP_CREATED) {
				
				InputStreamReader irObj = new InputStreamReader(postCon.getInputStream());
				BufferedReader br = new BufferedReader(irObj);
				String input = null;
				StringBuffer sb = new StringBuffer();
				while ((input = br.readLine()) != null) {
					sb.append(input);
				}
				br.close();
				postCon.disconnect();
				// printing the response
				
				System.out.println(sb.toString());
				new Group().show();
				
			} else {
					Alert alert = new Alert(Alert.AlertType.ERROR);
					alert.setTitle("Error");
					alert.setHeaderText(null);
					alert.setContentText("Combinations are not matched.");
					alert.showAndWait();
					return;
			}
				System.out.println("POST Request did not work.");
			}
	
		
	
	public void back(ActionEvent event) {
		new  Dashboard().show();
	}
}
