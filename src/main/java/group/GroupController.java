package group;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import dashboard.Dashboard;
import friends.Friends;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class GroupController {
	@FXML
	private TextField groupName;
	@FXML
	private TextField groupType;
	@FXML
	private Button createGroup;
	
	@FXML
	private Button groups;
	
	@FXML
	private Button friends;
	
	public void createGroup(ActionEvent event) throws IOException {
		if (groupName.getText().isEmpty() || groupType.getText().isEmpty() ) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText(" fill the all fields.");
			alert.showAndWait();
			return;
		}
		final String messageContent = "{\n" + "\"groupName\"" + ":\"" + groupName.getText() + "\", \r\n" + "\"grouptype\"" + ":\""
				+ groupType.getText() +  "\" \r\n"+ "\n}";
		
		System.out.println(messageContent);

		String link = "http://localhost:8080/groups/api/v1/create";
		URL url = new URL(link);
		HttpURLConnection postCon = (HttpURLConnection) url.openConnection();
		postCon.setRequestMethod("POST");
		postCon.setRequestProperty("userId", "abcdef");
		
		postCon.setRequestProperty("Content-Type", "application/json");
		postCon.setDoOutput(true);
		
		OutputStream os = postCon.getOutputStream();
		os.write(messageContent.getBytes());
		
		os.flush();
		os.close();
		int respCode = postCon.getResponseCode();
		System.out.println("Response from the server is: \n");
		System.out.println("The POST Request Response Code :  " + respCode);
		System.out.println("The POST Request Response Message : " + postCon.getResponseMessage());
		if (respCode == HttpURLConnection.HTTP_CREATED) {
			InputStreamReader irObj = new InputStreamReader(postCon.getInputStream());
			BufferedReader br = new BufferedReader(irObj);
			String input = null;
			StringBuffer sb = new StringBuffer();
			while ((input = br.readLine()) != null) {
				sb.append(input);
			}
			br.close();
			postCon.disconnect();
			 
			System.out.println(sb.toString());
			
			 new Group().show();
		} else {
			System.out.println("POST Request did not work.");
		
	}

	
  
	}
	public void groups(ActionEvent event) {
		new  Group().show();
	}
	public void friends(ActionEvent event) {
		new  Friends().show();
	}
	public void back(ActionEvent event) {
		new  Dashboard().show();
	}
}
