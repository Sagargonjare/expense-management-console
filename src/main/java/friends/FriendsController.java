package friends;

import dashboard.Dashboard;
import group.Group;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class FriendsController {
	@FXML
	private TextField fullName;
	
	@FXML
	private TextField email;
	
	@FXML
	private TextField mobile;
	
	@FXML
	private Button addFriend;

	@FXML
	private Button back;

	@FXML
	private Button friends;

	@FXML
	private Button groups;
	
	public void back(ActionEvent event) {
		new Dashboard().show();
		
	}
	public void addFriend(ActionEvent event) {
		new  Friends().show();
	}
	public void friends(ActionEvent event) {
		new Friends().show();
		
	}
	public void groups(ActionEvent event) {
		new  Group().show();
	}
}
